.. _contributors:

************
Contributors
************

Acknowledgement
----------------

The development of BigDAWG was supported by the Intel Science and Technology Center (ISTC) for Big Data. The authors also wish to thank collaborators as Brown University, University of Washington, Portland State University, Universtiy of Tennessee and Intel for their collaboration. 

Contributors
------------

There are a number of people involved in developing the current version of the codebase:

Adam Dziezdzic

Aaron Elmore

Vijay Gadepally

Jeremy Kepner

Kyle O'Brien

Sam Madden

Timothy Mattson

Jennie Rogers

Mike Stonebraker

Zuohao She


Alumni/Collaborators
--------------------

We are fortunate to have a number of collaborators who have helped us along the way:

Magdalena Balazinska, University of Washington

Leilani Battle, MIT CSAIL

Ugur Cetintemel, Brown University

Peinan Chen, MIT CSAIL

Ankush Gupta, MIT CSAIL

Brandon Haynes, University of Washington

Jeffrey Heer, University of Washington

Bill Howe, University of Washington

Tim Kraska, Brown University

David Maier, Portland State University

Stavros Papadopoulos, Intel

Jeff Parkhurst, Intel

Surabhi Ravishankar, Northwestern University

Ran Tan, North Carolina State University

Nesime Tatbul, Intel and MIT

Kristin Tufte, Portland State University

Manasi Vartak, MIT CSAIL

Katherine Yu, MIT CSAIL

Stan Zdonik, Brown University





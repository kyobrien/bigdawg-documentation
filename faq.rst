.. _faq:

Frequently Asked Questions
=============================

1.) What is BigDAWG?:

  BigDAWG (short for Big Data Working Group) is a reference
  implementation of a `Polystore database <http://wp.sigmod.org/?p=1629>`_. Essentially, BigDAWG
  provides the middleware needed to talk to multiple disparate engines
  (for example, SQL, NoSQL and NewSQL engines) while using multiple
  data model and programming languages (for example, SQL, AFL,
  AQL). More details about BigDAWG can be found in our publications.

2.) Where do I download get started and download everything I need to
see what you have released?:

  See :ref:`getting-started` for details.

3.) How do I modify the queries or make my own?:

  See Section :ref:`query-language`

4.) How do I add my own engine?:

  See Section :ref:`query-language` and contact us for help! Perhaps
  someone has already integrated (or is in the process of integrating) the engine of interest.

5.) How do I add my own data/tables?:

  We've distributed a handy Python script that can help you load
  data. See :ref:`administration` for details on how to use this. If
  you have more questions, of course, email us!

6.) What is the query API?:

  Section :ref:`query-language` addresses the query language we use.

7.) How do I create a new island?:

  Looks at Section :ref:`administration` for insight on how to do this. Please feel free to reach out to us if you have any other questions or comments.
  
8.) How do I contact the development team with bugs, questions, etc.?
 
  Email us at ``bigdawg-help@mit.edu``
  
9.) How is BigDAWG licensed?

  The BigDAWG middleware is licensed under the terms of the BSD 3-clause license. Please note that external componenents such as database management engines may have their own license agreements. Please reach out to us for specific licensing questions.

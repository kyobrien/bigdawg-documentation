.. _publications:

=============================
Selected BigDAWG Publications
=============================

Overall architecture:
---------------------

“The BigDAWG Polystore System and Architecture”, 
Vijay Gadepally, Peinan Chen,  Jennie Duggan,  Aaron Elmore, Brandon Haynes,  
Jeremy Kepner, Samuel Madden,  Tim  Mattson, Michael Stonebraker.
IEEE High Performance Extreme Computing, 2016.    

     BigDAWG overall architecture and details of various middleware
     components along with some performance results.


"The Big Dawg Polystore System",
J. Duggan, A. J. Elmore, M. Stonebraker, M. Balazinska, B. Howe, 
J. Kepner, S. Madden, D. Maier, T. Mattson, S. Zdoânik. 
ACM Sigmod Record, 44(3), 2015.

     Original vision paper on BigDAWG architecture.


BigDAWG applications:
---------------------

"Demonstrating the BigDAWG Polystore System for Ocean Metagenomic Analysis",
Tim Mattson, Vijay Gadepally, Zuohao She, Adam Dziedzic, Jeff Parkhurst
CIDR’17 Chaminade, CA, USA

     This paper describes a second application based on BigDAWG; an 
     oceanography dataset including integration with the S-Store
     system for streaming data.


"A Demonstration of the BigDawg Polystore System", 
A. Elmore, J. Duggan, M. Stonebraker, U. Cetintemel, V. Gadepally,
J. Heer, B. Howe, J. Kepner, T. Kraska, S. Madden, D. Maier,
T. Mattson, S. Papadopoulos, J. Parkhurst, N. Tatbul, M. Vartak, S. Zdonik.
Proceedings of VLDB, 2015.

     This paper describes our performance measurements with the MIMCII dataset.


BigDAWG Middleware:
-------------------

"The BigDAWG Monitoring Framework", 
Peinan Chen, Vijay GAdepally, Michael Stonebraker
IEEE High Performance Extreme Computing, 2016.

     This paper describes the BigDAWG monitoring framework.

"BigDAWG Polystore Query Optimization Through Semantic Equivalences",
Zuohao She, Surabhi Ravishankar, Jennie Duggan 
IEEE High Performance Extreme Computing, 2016.    

     This paper describes query optimization in BigDAWG.

"Cross-Engine Query Execution in Federated Database Systems", 
Ankush M. Gupta, Vijay Gadepally, Michael Stonebraker (MIT)
IEEE High Performance Extreme Computing, 2016.    

     This paper describes how queries are split between different islands.

"Data Transformation and Migration in Polystores",
Adam Dziedzic, Aaron J. Elmore, Michael Stonebraker

     This paper describes how the casts work in BigDAWG.

"Integrating Real-Time and Batch Processing in a Polystore",
John Meehan, Stan Zdonik Shaobo Tian, Yulong Tian, 
Nesime Tatbul, Adam Dziedzic, Aaron Elmor

     This paper provides details behind the integration of the 
     S-Store streaming system with BigDAWG.

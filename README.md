## Welcome to BigDawg documentation repository. 

Rendered documentation: [http://bigdawg-documentation.readthedocs.io/en/latest/index.html](http://bigdawg-documentation.readthedocs.io/en/latest/index.html)

Build status: [https://readthedocs.org/projects/bigdawg-documentation/builds/](https://readthedocs.org/projects/bigdawg-documentation/builds/)

## For documentation authors:

This repository is written in reStructuredText (reST), a markup language similar to Markdown. Each time the master branch is pushed to, ReadTheDocs will automatically render the HTML and host it at [http://bigdawg-documentation.readthedocs.io/en/master/](http://bigdawg-documentation.readthedocs.io/en/master/)

Here is a reStructuredText markup guide: http://www.sphinx-doc.org/en/1.5.1/rest.html

Here�s a general guide to using reST for ReadTheDocs: http://restructuredtext.readthedocs.io/en/latest/sphinx_tutorial.html

## Fixing the latex source to include chapters:

After rendering the latex with ``make latex``, replace all "\subsection{" with "\chapter{" below:

* \subsection{Introduction and Overview}
* \subsection{Getting Started with BigDAWG}
* \subsection{BigDAWG Middleware Internal Components}
* \subsection{BigDAWG Query Language}
* \subsection{Personalizing the setup}
* \subsection{Selected BigDAWG Publications}
* \subsection{Frequently Asked Questions}

Do a replace all 2x: "subsection{" replace with "section{"

